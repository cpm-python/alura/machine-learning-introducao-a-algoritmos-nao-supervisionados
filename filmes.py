# -*- coding: utf-8 -*-
"""filmes.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1QEjvU3brd7qCjFOUXRE3gmq3FV638o0M
"""

print('Olá mundo')

import pandas as pd

uri_filmes = 'https://raw.githubusercontent.com/oyurimatheus/clusterirng/master/movies/movies.csv'
filmes =  pd.read_csv(uri_filmes)
filmes.head()

filmes.columns = ['filme_id', 'titulo', 'generos']
filmes.head()

generos = filmes.generos.str.get_dummies()
generos.head()

dados_filmes = pd.concat([filmes, generos], axis=1)
dados_filmes.head()

from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
generos_escalados = scaler.fit_transform(generos)
generos_escalados.shape

from sklearn.cluster import KMeans
modelo = KMeans(n_clusters=3)
modelo.fit(generos_escalados)

print(f'Grupos: {modelo.labels_}')

print(generos.columns)
print(modelo.cluster_centers_)

grupos = pd.DataFrame(modelo.cluster_centers_, columns=generos.columns)

grupos

grupos.transpose().plot.bar(subplots=True, figsize=(25, 25), sharex = False)

grupo = 1
filtro = modelo.labels_ == grupo
dados_filmes[filtro].sample(10)

from sklearn.datasets import make_blobs
dados, _ = make_blobs(n_samples=1000, n_features=2, random_state=7)
dados = pd.DataFrame(dados, columns=['coluna1', 'coluna2'])
dados.head()

import matplotlib.pyplot as plt

plt.scatter(x=dados.coluna1, y=dados.coluna2)

modelo = KMeans(n_clusters=3)
grupos = modelo.fit_predict(dados)
plt.scatter(x=dados.coluna1, y=dados.coluna2, 
            c=grupos,
           cmap='viridis')
plt.scatter(centroides[:, 0], centroides[:, 1],
           marker='X', s=169, linewidths=5,
           color='g', zorder=8)

from sklearn.manifold import TSNE
tsne = TSNE()
visualizacao = tsne.fit_transform(generos_escalados)
visualizacao

import seaborn as sns

sns.set(rc={'figure.figsize' : (13, 13)})

sns.scatterplot(x=visualizacao[:, 0],
               y=visualizacao[:, 1],
#                hue=modelo.labels_,
               palette=sns.color_palette('Set1', 3))

modelo = KMeans(n_clusters=20)
modelo.fit(generos_escalados)
grupos = pd.DataFrame(modelo.cluster_centers_, columns=generos.columns)

grupos.head()

grupos.transpose().plot.bar(subplots=True,
                            figsize=(25, 50),
                            sharex = False,
                           rot=0)

grupo = 17
filtro = modelo.labels_ == grupo
dados_filmes[filtro].sample(10)

def kmeans(numero_de_clusters, generos):
  modelo = KMeans(n_clusters=numero_de_clusters)
  modelo.fit(generos)
  return [numero_de_clusters, modelo.inertia_]

kmeans(20, generos_escalados)

kmeans(3, generos_escalados)

resultado = [kmeans(numero_de_grupos, generos_escalados) for numero_de_grupos in range(1,41)]
resultado

resultado = pd.DataFrame(resultado, columns=['grupos', 'inertia'])
resultado.head()

resultado.inertia.plot(xticks=resultado.grupos)

modelo = KMeans(n_clusters=14)
modelo.fit(generos_escalados)

grupos = pd.DataFrame(modelo.cluster_centers_, columns=generos.columns)

grupos.transpose().plot.bar(subplots=True,
                            figsize=(25, 50),
                            sharex = False,
                           rot=0)

grupo = 7
filtro = modelo.labels_ == grupo
dados_filmes[filtro].sample(10)

from sklearn.cluster import AgglomerativeClustering

modelo = AgglomerativeClustering(n_clusters=14)
grupos = modelo.fit(generos_escalados)
grupos

tsne = TSNE()
visualizacao = tsne.fit_transform(generos_escalados)
visualizacao

sns.scatterplot(x=visualizacao[:, 0],
               y=visualizacao[:, 1],
                hue=grupos)

grupos

from scipy.cluster.hierarchy import dendrogram, linkage

modelo = KMeans(n_clusters=14)
modelo.fit(generos_escalados)

grupos = pd.DataFrame(modelo.cluster_centers_, columns=generos.columns)

grupos.transpose().plot.bar(subplots=True,
                            figsize=(25, 50),
                            sharex = False,
                           rot=0)

matriz_de_distancia = linkage(grupos)
matriz_de_distancia

dendograma = dendrogram(matriz_de_distancia)